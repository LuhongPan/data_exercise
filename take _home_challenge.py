# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 11:28:50 2021

@author: PanL
"""

# 01.Conversion Rate
from sklearn.model_selection import train_test_split
train_features, test_features, train_labels, test_labels = train_test_split(X, y, test_size = 0.3, random_state = 42, stratify=y)

for column in data.columns:
    uniques = sorted(data[column].unique())
    print('{0:20s} {1:5d}\t'.format(column, len(uniques)), uniques[:5])


data.info()
data.describe()
data.isnull().sum()

## Visualization of different countries
grouped = data[['country', 'converted']].groupby('country').mean().reset_index()

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(18, 6))
sns.countplot(x='country', hue='converted', data=data, ax=ax[0])
ax[0].set_title('Count Plot of Country', fontsize=16)
ax[0].set_yscale('log')
sns.barplot(x='country', y='converted', data=data, ax=ax[1]);
ax[1].set_title('Mean Conversion Rate per Country', fontsize=16)
plt.tight_layout()
plt.show()

## Visualization of different sources
grouped = data[['age', 'converted']].groupby('age').mean().reset_index()
hist_kws={'histtype': 'bar', 'edgecolor':'black', 'alpha': 0.2}

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(18, 6))
sns.distplot(data[data['converted'] == 0]['age'], label='Converted 0',
             ax=ax[0], hist_kws=hist_kws)
sns.distplot(data[data['converted'] == 1]['age'], label='Converted 1',
             ax=ax[0], hist_kws=hist_kws)
ax[0].set_title('Count Plot of Age', fontsize=16)
ax[0].legend()
ax[1].plot(grouped['age'], grouped['converted'], '.-')
ax[1].set_title('Mean Conversion Rate vs. Age', fontsize=16)
ax[1].set_xlabel('age')
ax[1].set_ylabel('Mean convertion rate')
ax[1].grid(True)
plt.show()



# 02.TranslateABTest
# Separate stay and quit data
quit_data = data[~data['quit_date'].isnull()]
stay_data = data[data['quit_date'].isnull()]



# 03. Employee Retention
# 04. Identifying Fraudulent Activities
# 05. Funnel Analysis
# 06. Pricing Test.ipynb
def group_statistics(df):
    return pd.Series({'n_users': df.shape[0],
                      'convert_rate': df.converted.mean(),
                      'mean_revenue': (df.price * df.converted).mean()})
testdata.groupby('test').apply(group_statistics)
## Chi-Square test for feature selection(when the feature is categorical It measures the degree of association between two categorical variables, the target variable is any way can be thought as categorical
## If both are numeric, we can use Pearson’s product-moment correlation,
## and if the attribute is numerical and there are two classes we can use a t-test if more than two classes we can use ANOVA)
colnames = ["source","device","OS",'price']
ch2values,pvalues = chi2(X.loc[:,colnames],X["converted"])
pd.DataFrame({'chi2_value':ch2values,'pvalue':pvalues},index = colnames).sort_values(by='pvalue')

dt = DecisionTreeClassifier(max_depth=4)
dt.fit(Xtrain,ytrain)
export_graphviz(dt,feature_names=Xtrain.columns,proportion=True,leaves_parallel=True)
pd.Series(dt.feature_importances_,index = Xtrain.columns).sort_values(ascending=False)



# 07. Marketing Email Campaign
chi2scores,_ = chi2(X,y)
fscores,_ = f_classif(X,y)
feat_scores = pd.DataFrame({"chi2scores":chi2scores,"fscores":fscores},index=feat_names)



# 08. Song Challenge
def find_first_user(df):
    """ function to find the first user """
    idx = df['user_sign_up_date'].argmin()
    return df.loc[idx, ['user_id', 'user_sign_up_date']]

# Step 1: build the Song-User matrix
song_user = data.groupby(['song_played', 'user_id'])['id'].count().unstack(fill_value=0)

# Step 2: build song-song similarity matrix
song_user_norm = normalize(song_user, axis=1)  # normalize the song-user matrix
similarity = np.dot(song_user_norm, song_user_norm.T)  # calculate the similarity matrix
similarity_df = pd.DataFrame(similarity, index=song_user.index, columns=song_user.index)

# Step 3: find the top-k most similar songs
def find_topk(song, similarity, k=1):
    df = similarity.loc[song].sort_values(ascending=False)[1:k + 1].reset_index()
    df = df.rename(columns={'song_played': 'Song', song: 'Similarity'})
    return df



# 09. Clustering Grocery Items
item_user_most = user_item_count.apply(np.argmax, axis=0).reset_index()
item_user_most = item_user_most.rename(columns={'index': 'Item_id', 0: 'User_id'})
# or the below way
max_user_byitem = user_item_counts.apply(lambda s: pd.Series([s.argmax(), s.max()], index=["max_user", "max_count"]))
# determine the best number of clusters
clusters = range(2, 30)
inertias = []
silhouettes = []

for n_clusters in clusters:
    kmeans = KMeans(n_clusters=n_clusters, init='k-means++', random_state=42, n_jobs=-1)
    kmeans = kmeans.fit(feature)
    label = kmeans.predict(feature)
    inertias.append(kmeans.inertia_)
    silhouettes.append(silhouette_score(feature, label))

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(18, 6))
ax[0].plot(clusters, inertias, 'o-', label='Sum of Squared Distances')
ax[0].grid(True)
ax[1].plot(clusters, silhouettes, 'o-', label='Silhouette Coefficient')
ax[1].grid(True)
plt.legend(fontsize=12)
plt.tight_layout()
plt.show()



# 10. Credit Card Transactions
## Isolation Forest https://towardsdatascience.com/outlier-detection-with-isolation-forest-3d190448d45e

## 11. User Referral Program
pd.DF.xs用法
## 12. Loan Granting
#when data is imbalanced, use AUC as the objective function rather than accuracy
# https://github.com/stasi009/TakeHomeDataChallenges/blob/master/12.LoanGrant/loan_grant.ipynb
def train(params):
    params['silent'] = 1
    params['objective'] = 'binary:logistic'  # output probabilities
    params['eval_metric'] = 'auc'

    num_rounds = params["num_rounds"]
    early_stopping_rounds = params["early_stop_rounds"]

    # early stop will check on the last dataset
    watchlist = [(train_matrix, 'train'), (valid_matrix, 'validate')]
    bst = xgb.train(params, train_matrix, num_rounds, watchlist, early_stopping_rounds=early_stopping_rounds)

    print "parameters: {}".format(params)
    print "best {}: {:.2f}".format(params["eval_metric"], bst.best_score)
    print "best #trees: %d" % (bst.best_ntree_limit)

    return bst

# ------------- prepare parameters
params = {}
params["num_rounds"] = 300
params["early_stop_rounds"] = 30
# params['min_child_weight'] = 2
params['max_depth'] = 6
params['eta'] = 0.1
params["subsample"] = 0.8
params["colsample_bytree"] = 0.8

# ------------- train with xgboost
bst = train(params)
n_trees = bst.best_ntree_limit
n_trees

yvalid_true = valid_matrix.get_label()
yvalid_pred_probas = bst.predict(valid_matrix, ntree_limit=bst.best_ntree_limit)

fpr,tpr,thresholds = roc_curve(yvalid_true,yvalid_pred_probas)
roc = pd.DataFrame({'FPR':fpr,'TPR':tpr,'Thresholds':thresholds})

def calc_profits(repaids,probas,threshold):
    total_profit = 0
    for (repaid,proba) in itertools.izip(repaids,probas):
        if proba > threshold:
            # it is possible for repaid=NaN
            # which means, my model grant loan to a borrower whose application was rejected under old model
            # the question doesn't clarify how to calculate profit for such case.
            # however, since most 'rejected borrowers' were considered unlikely to repay by old model
            # so I also think such people are very unlikely to repay even were granted
            # so their profit are also -1, the same as 'granted but unable to repay'
            total_profit += (1 if repaid == 1 else -1)
    return total_profit

# all examples in validation sets
loan_valid = loan.loc[yvalid.index,:]

# calculate profits under each candidate cutoff
valid_profits = [ calc_profits(loan_valid.repaid,yvalid_pred_probas,threshold) for threshold in roc.Thresholds]

plt.plot(roc.Thresholds,valid_profits)
plt.yticks(xrange(-10000,4000,1000))
_ = plt.xticks(np.linspace(0,1,11))
plt.rc('figure',figsize=(5,5))

valid_threshold_profits = pd.DataFrame({'threshold':roc.Thresholds,'profit':valid_profits})
temp = valid_threshold_profits.loc[  valid_threshold_profits.profit.argmax()   ,:]
temp



# 13. Json City Similarities
hours_by_country = sessions.groupby("user_country").apply(lambda df: df.timestamp.dt.hour.value_counts(normalize=True)).unstack(fill_value=0)


# 14. Optimization of Employee Shuttle Stops
# 15. Diversity in the Workplace
# LGBM
# 16. URL Parsing Challenge
# 17. Engagement Test
# 18. On-Line Video Challenge
rch_bins = [0,0.95,1.05,100]
vstatistics['trend_status'] = pd.cut(vstatistics.rch_mean,rch_bins,right=False,labels=['decrease','flat','increase'])


# 19. Subscription Retention Rate
## for each row in 'count_by_cost', we perform a reverse cumsum to get the #people by the end of each billing cycles
total_by_cost = count_by_cost.apply(lambda s: s.iloc[::-1].cumsum().iloc[::-1],axis=1).transpose()


# 20. Ads Analysis


# *  https://stats.stackexchange.com/questions/81659/mutual-information-versus-correlation
# * https://nlp.stanford.edu/IR-book/html/htmledition/mutual-information-1.html
# * https://pbpython.com/groupby-agg.html
